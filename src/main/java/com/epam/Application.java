package com.epam;

import java.util.Scanner;

/**
 * The Application program prints odd numbers from start to the end of interval,
 * and even from end to start.
 * Program prints the sum of odd and even numbers.
 *
 * @author Volodymyr Danylko
 * @version 1.0
 * @since 2019-11-10
 */

public class Application {

    /**
     * This method prints odd numbers from start to the end of interval,
     * end sum of odd numbers.
     *
     * @param startInterval This is the start interval parameter
     * @param endInterval   This is the end interval parameter
     */
    private static void printOddNumbers(int startInterval, int endInterval) {
        int sumOdd = 0;
        for (int i = startInterval; i <= endInterval; i++) {
            if (i % 2 != 0) {
                sumOdd += i;
                System.out.print(i + " ");
            }
        }
        System.out.println("\nSum of odd numbers: " + sumOdd);
    }

    /**
     * This method prints even numbers from end to the start of interval,
     * end sum of even numbers.
     *
     * @param startInterval This is the start interval parameter
     * @param endInterval   This is the end interval parameter
     *
     */

    private static void printEvenNumbers(int startInterval, int endInterval) {
        int sumEven = 0;
        for (int i = endInterval; i >= startInterval; i--) {
            if (i % 2 == 0) {
                sumEven += i;
                System.out.print(i + " ");
            }
        }
        System.out.println("\nSum of even numbers: " + sumEven);
    }


    /**
     * @param args Unused
     */

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a start interval number: ");
        int startInterval = in.nextInt();
        System.out.print("Input a end interval number: ");
        int endInterval = in.nextInt();


        Application.printEvenNumbers(startInterval, endInterval);
        Application.printOddNumbers(startInterval, endInterval);


        System.out.println("Please enter the size of Fibonacci set: ");
        int sizeFibonacci = in.nextInt();

        Fibonacci.getFibonacci(sizeFibonacci);


    }
}
