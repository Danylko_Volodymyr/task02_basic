package com.epam;

/**
 * The Fibonacci static class build and print Fibonacci numbers.
 * Program prints percentage of odd and even Fibonacci numbers
 *
 */

class Fibonacci {
    /**
     * This is recursive method used to generate Fibonacci number.
     *
     * @param i This is parameter to Fib method
     * @return Fibonacci number
     */
    private static int fib(int i) {
        if (i == 1) {
            return 1;
        }
        if (i == 2) {
            return 1;
        }
        return fib(i - 1) + fib(i - 2);
    }

    /**
     * This method used to set array of Fibonacci numbers.
     *
     * @param n This is parameter to Fib method
     * @return Array of Fibonacci numbers
     */

    private static int[] setFib(int n) {
        int[] arr = new int[n];
        for (int i = 1; i < arr.length; i++) {
            arr[i] = fib(i);
        }
        return arr;
    }

    /**
     * This method used to print array of Fibonacci numbers,
     * and statistic.
     *
     * @param n This is parameter to getFibonacci method
     */

    public static void getFibonacci(int n) {
        int[] arr = setFib(n);
        int F1 = 0;
        int F2 = 0;
        double countF1 = 0;
        double countF2 = 0;
//        for (int i = 1; i < arr.length; i++) {
//            arr[i] = fib(i);
//        }
        System.out.println("Fibonacci numbers");
        for (int item : arr
        ) {
            if (item % 2 != 0) {
                countF1++;
            }
            if (item % 2 == 0) {
                countF2++;
            }
            if (item > F1 && item % 2 != 0) {
                F1 = item;
            }
            if (item > F2 && item % 2 == 0) {
                F2 = item;
            }

            System.out.print(item + " ");
        }
        System.out.println("\nF1: " + F1);
        System.out.println("F2: " + F2);
        System.out.println("Odds %: " + (countF1 / n) * 100);
        System.out.println("Evens %: " + (countF2 / n) * 100);
    }
}
